<?php

namespace Tests\Feature;

use App\Models\User;
use Tests\TestCase;

class ExampleTest extends TestCase
{
    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_example()
    {
        User::factory()->create();

        $this->actingAs(User::all()->first());
        $response = $this->get('/dashboard');

        $response->assertOk();
        $response->assertStatus(200);

        $response->assertSee("You're logged in!", false);

    }
}
